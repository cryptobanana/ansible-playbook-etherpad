#!/bin/bash
set -x
#set -e

docker rm -f ansible_base
docker run --name ansible_base -p 5000:22 -h ansible_base -d ansible_base_ubuntu:latest
ansible-playbook -i hosts_docker playbook.yml -vv
